#!/bin/bash

# Dependencies:
# see https://gitlab.com/Linaro/tuxmake
#
# You have to stand in the kernel tree to run this script.
# take whatever is in the tuxmake_reproducer.sh and pass that after this script 'bisect.sh'.
# NEW=next-20201210 OLD=v5.10-rc7-21-g69fe24d1d80f bisect.sh tuxmake -q --build-dir=$(pwd)/bisect-build-output --runtime docker --target-arch sh --toolchain gcc-10 --kconfig defconfig

TUXMAKE_CMD="${TUXMAKE_CMD:-"$@"}"
export TUXMAKE_IMAGE_REGISTRY=public.ecr.aws


if [[ -z "${NEW}" ]]; then
	echo "need to set NEW"
	exit 1
fi

if [[ -z "${OLD}" ]]; then
	echo "need to set OLD"
	exit 1
fi

echo "Start bisecting..."
git checkout ${NEW}
git bisect start $(git rev-parse ${NEW}) $(git rev-parse ${OLD})

git bisect run $TUXMAKE_CMD 2>&1| tee bisect_output.log
git bisect log|tee bisect_${NEW}-${OLD}.log
first_bad_commit=$(cat "bisect_${NEW}-${OLD}.log"|tail -1|sed 's/.*\[//'|sed 's/\].*//')
git log -1 --stat $first_bad_commit| tee -a "bisect_${NEW}-${OLD}.log"
git bisect reset
git revert --no-edit $first_bad_commit
