#!/bin/bash

set -x

OLDSHA_FILE="${OLDSHA_FILE:-".oldsha"}"

if [[ -z "${OLD}" ]]; then

	echo "trying to find the old"
	count=32
	found_good_old=0

	while true; do
		count=$((${count}*2))
		OLD=$(git log -1 --format=format:%H ${NEW}~${count})
		git checkout ${OLD}
		$TUXMAKE_CMD
		if [[ $? -eq 0 ]]; then
			break
		fi
	done
fi
echo "$OLD" > ${OLDSHA_FILE}
